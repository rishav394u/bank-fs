#include <iostream>
#include <fstream>
#include <string.h>
#include <cstdlib>
#include <conio.h>

using namespace std;

class donor
{

public:
	char id[10];							//client Id
	char first_name[20];			//client First name
	char last_name[20];				//client Last name
	char amount_of_blood[10]; //client amount_of_blood
	char blood_type[10];			// Blood group of the client

	//read function
	void read()
	{
		cout << "Enter the id: " << endl;
		cin >> id;
		cout << "Enter the first name " << endl;
		cin.ignore();
		cin.getline(first_name, 21);
		cout << "Enter the last name " << endl;
		cin >> last_name;
		cout << "Enter the total amount of blood donated " << endl;
		cin >> amount_of_blood;
		cout << "Enter the blood group " << endl;
		cin >> blood_type;
	}
};

fstream f;
fstream file2;

//insert donor inside the file
void insert_donor()
{
	char c;
	donor p;

	do
	{
		p.read();
		f.open("client.txt", ios::in | ios::out | ios::app);
		if (f.is_open())
		{
			f << p.id << "|";
			f << p.first_name << "|";
			f << p.last_name << "|";
			f << p.amount_of_blood << "|";
			f << p.blood_type << "\n";

			f.close();
		}

		else
		{
			cout << "Error opening the file " << endl;
		}

		cout << "Do you want to Insert again? (y/n) " << endl;
		cin >> c;
	}

	while (c == 'y');
}

//get data from the file
void get_donor()
{
	donor p;

	f.open("client.txt", ios::in | ios::out | ios::app);

	if (f.is_open())
	{
		while (!f.eof())
		{
			f.getline(p.id, 11, '|');
			cout << p.id << "\t";

			f.getline(p.first_name, 21, '|');
			cout << p.first_name << "\t";

			f.getline(p.last_name, 21, '|');
			cout << p.last_name << "\t";

			f.getline(p.amount_of_blood, 11, '|');
			cout << p.amount_of_blood << "\t";

			f.getline(p.blood_type, 11, '|');
			cout << p.blood_type << "\n";
		}

		f.close();
	}
	else
	{
		cout << "Error opening file " << endl;
	}
}

//search
void search_id()
{
	donor p;
	char c;
	char iid[10];
	do
	{
		f.open("client.txt", ios::in);
		cout << "Enter the id to search ... " << endl;
		cin >> iid;
		int flag = 0;
		while (!f.eof())
		{
			//cout<<"Done \n";
			f.getline(p.id, 11, '|');
			f.getline(p.first_name, 21, '|');
			f.getline(p.last_name, 21, '|');
			f.getline(p.amount_of_blood, 11, '|');
			f.getline(p.blood_type, 11, '\n');

			if (strcmp(iid, p.id) == 0)
			{
				flag = 1;

				cout << "Id \t :" << p.id << "\n"
						 << "First name \t :" << p.first_name << "\n"
						 << "Last name \t :" << p.last_name << "\n"
						 << "Net amount of blood donated \t :" << p.amount_of_blood << "\n"
						 << "Blood Group \t :" << p.blood_type << endl;
				break;
			}
		}
		if (flag == 0)
		{
			cout << "Not found " << endl;
		}
		f.close();
		cout << "Are you sure to search again ? (y/n)?" << endl;
		cin >> c;

		if (c != 'y' && c != 'n')
		{
			cout << "You entered a wrong choice please enter Y or N " << endl;
			cin >> c;
		}

	} while (c == 'y');
}

//Update_all Data
void update_donor(char iid[10])
{
	donor p;
	f.open("client.txt", ios::in);
	file2.open("help.txt", ios::out | ios::app);
	int flag_e = 1;
	int numprocess;
	//the new elements
	char newid[10];
	char newf_name[20];
	char newl_name[20];
	char newamount_of_blood[10];
	char newtrust[10];

	cout << "1: Update All Data " << endl;
	cin >> numprocess;

	if (numprocess == 1)
	{
		cout << "Enter your new ID " << endl;
		cin >> newid;
		cout << "Enter your new First name " << endl;
		cin >> newf_name;
		cout << "Enter your new last name " << endl;
		cin >> newl_name;
		cout << "Enter your new net amount of blood donated " << endl;
		cin >> newamount_of_blood;
		cout << "Enter your new Cash " << endl;
		cin >> newtrust;
	}
	else
	{
		cout << "Please Enter 1 to update all data " << endl;
	}

	while (!f.eof())
	{
		f.getline(p.id, 11, '|');
		f.getline(p.first_name, 21, '|');
		f.getline(p.last_name, 21, '|');
		f.getline(p.amount_of_blood, 11, '|');
		f.getline(p.blood_type, 11, '\n');

		if (numprocess == 1)
		{

			if (strcmp(p.id, iid) != 0)
			{
				cout << "No such donor found. Making a new donor." << endl;
				file2 << p.id << "|" << p.first_name << "|" << p.last_name << "|" << p.amount_of_blood << "|" << p.blood_type << endl;
			}

			else
			{
				file2 << newid << "|" << newf_name << "|" << newl_name << "|" << newamount_of_blood << "|" << newtrust << endl;
				flag_e = 0;
			}
		}
	}

	file2.close();
	f.close();

	if (flag_e != 0)
	{
		cout << "NOT FOUNDED \n\n";
	}

	else
	{
		cout << "THE RECORD IS UPDATED " << endl;
	}

	remove("client.txt");
	rename("help.txt", "client.txt");
}

//Delete
void delete_donor(char iid[10])
{
	donor p;

	f.open("client.txt", ios::in);

	file2.open("help.txt", ios::out | ios::app);

	int flag_e = 1;

	while (!f.eof())
	{
		f.getline(p.id, 11, '|');
		f.getline(p.first_name, 21, '|');
		f.getline(p.last_name, 21, '|');
		f.getline(p.amount_of_blood, 11, '|');
		f.getline(p.blood_type, 11, '\n');

		if (strcmp(p.id, iid) != 0)
		{
			file2 << p.id << "|" << p.first_name << "|" << p.last_name << "|" << p.amount_of_blood << "|" << p.blood_type << endl;
		}
		else
		{
			flag_e = 0;

			cout << "\nId"
					 << "  "
					 << "First name"
					 << "  "
					 << "Last name"
					 << "  "
					 << "Net amount of blood"
					 << "  "
					 << "Blood group" << endl;
			cout << p.id << "\t" << p.first_name << "\t     " << p.last_name << "\t     " << p.amount_of_blood << "\t     " << p.blood_type << endl;
		}
	}

	file2.close();
	f.close();

	if (flag_e != 0)
	{
		cout << "NOT FOUND\n\n";
	}
	else
	{

		cout << "THE RECORD IS DELETED " << endl;
	}

	remove("client.txt");
	rename("help.txt", "client.txt");
}

void handle_cases(int number)
{
	switch (number)
	{

	case 1:
		insert_donor();
		break;

	case 2:
		get_donor();
		break;

	case 3:
		search_id();
		break;

	case 4:
		char x;
		do
		{
			char iidx[10];
			cout << "Enter ID to update : " << endl;
			cin >> iidx;
			update_donor(iidx);
			cout << "Do you want to update another donor? (y/n)? " << endl;
			cin >> x;
		} while (x == 'y');

		break;

	case 5:
		char c;
		do
		{
			char donor_id[10];
			cout << "Enter ID to Delete : " << endl;
			cin >> donor_id;
			delete_donor(donor_id);

			cout << "Do you want to Delete again ? (y/n)? " << endl;
			cin >> c;
		} while (c == 'y');
		break;

	default:
		cout << "Invalid choice. Please re enter " << endl;
	}
}

void start_main()
{
	char c;
	do
	{
		system("cls");
		cout << "Welcome to blood bank management system:\n\n";
		cout << "************************************************************************\n";
		cout << "     1:Insert new donor "
				 << "\n";
		cout << "     2:Read all donors "
				 << "\n";
		cout << "     3:Search donor "
				 << "\n";
		cout << "     4:Update any donor details (Donate more blood / Fix entry) "
				 << "\n";
		cout << "     5:Delete donor "
				 << "\n";
		cout << "************************************************************************\n";
		cout << "PLEASE ENTER YOUR CHOICE: " << endl;
		;

		int number;
		cin >> number;
		system("cls");
		handle_cases(number);
		cout << "Press any key to continue or n to exit? (y/n)" << endl;
		cin >> c;
	}

	while (c != 'n');
}

int main()
{
	start_main();
	return 0;
}
